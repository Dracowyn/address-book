/**
 * 获取地址的参数
*/
function getParams(name)
{
    let query = decodeURI(window.location.search.substring(1));

    // 以&符号作为分隔符
    let vars = query.split('&');

    for(let i = 0;i < vars.length;i++)
    {
        let pair = vars[i].split('=');

        if(pair[0] == name)
        {
            return pair[1];
        }
    }

    return null;
}