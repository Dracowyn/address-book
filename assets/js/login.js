// 验证登录 有些页面需要登录后才访问
$(async function(){
    let business = cookie.get('business') ? JSON.parse(cookie.get('business')) : {};

    if(JSON.stringify(business) === '{}')
    {
        mui.toast('请先登录');
        setTimeout(() => {
            location.href = 'login.html';
        },2000);
        return;
    }

    // 组装数据
    let data = {
        id: business.id ? business.id : 0,
        mobile: business.mobile ? business.mobile : ''
    }

    // 发起请求
    let result = await POST({
        url:'business/check',
        params:data
    });

    // 如果cookie信息是无效的
    if(result.code === 0)
    {
        cookie.remove('business');
        mui.toast('非法登录');
        setTimeout(() => {
            location.href = 'login.html';
        },2000);

        return;
    }

    // 更新cookie
    cookie.set('business',JSON.stringify(result.data));
}())